# LDAP SERVER
## @isx43567395
### Servidor LDAP

Repositorio ldap 2020

Podéis encontrar las imagenes docker al dockerhub [zeuslawl] (https://hub.docker.com/u/zeuslawl/)

ASIX M06-ASO Escola del treball de barcelona

Imagen:
**isx43567395/phpldapadmin Imagen con un servidor phpldapadmin. Conecta al servidor ldap nombrado como ldap.edt.org. 
Para acceder a las bases dc=edt,dc=org i cn=config. Esta imagen está basada en fedora:27 para evitar cambios en la sintaxis de PHP 7.4

detach:

$ docker run --rm  --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisix -p 80:80 -d zeuslawl/phpldapadmin:20 

