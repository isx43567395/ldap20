# LDAP SERVER
## @isx43567395
### Servidor LDAP

Repositorio ldap 2020

Podéis encontrar las imagenes docker al dockerhub [zeuslawl] (https://hub.docker.com/u/zeuslawl/)

ASIX M06-ASO Escola del treball de barcelona

Imagen:

**isx43567395/ldap20_acl** Imagen para hacer pruebas de modificación de las acls usando ficheros de modificación. Se ha incorporado la BD cd=config para la administración del servidor dinamicamente.

interactivo:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it zeuslawl/ldap20:acl /bin/bash
```

Detach:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/ldap20:acl

Exemple:
access to * by * read
access to * by * write
access to * by self write by * read
-----------------------------
access to * 
	by self write 
	by * read
--------------------------
access to attrs=homePhone by * read
access to * by * write
--------------------------
access to attrs=homePhone
	by dn.exact="cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write
	by * read
access to * by * write
-----------------------

----------------------------------
Exemple 5 MAL, INCORRECTA, LA CORRECTA ES LA ANTERIOR.
access to attrs=homePhone
	by dn.exact="cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write
access to attrs=homePhone
	by * read
access to * by * write
---------------------------------

-------------------------------
Exemple 6
access to attrs=homePhone
	by dn.exact="cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write
	by dn.exact="cn=Admin System,ou=usuaris,dc=edt,dc=org" write
	by * read
access to *
	by dn.exact="cn=Admin System,ou=usuaris,dc=edt,dc=org" write
	by self write
	by * read
-----------------------

Exemple 7

access to attrs=userPassword
	by self write
	by * auth
access to *
	by * read 
---------------------


-----------
Comandos de pruebas
---------------------
pau cambia pass a anna
ldappasswd -v -x -D 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org' -w pau -s anna 'cn=Anna Pou,ou=usuaris,dc=edt,dc=org'
----------------------

-------------------
ldappasswd -v -x -D 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org' -w pau -s pau


Pau modifica su email
ldapmodify -x -D 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org' -w pau -f pr.mod1.ldif 

Anna mofifica email de pau
ldapmodify -x -D 'cn=Anna Pou,ou=usuaris,dc=edt,dc=org' -w anna -f pr.mod1.ldif 


Pau modifica su homephone
ldapmodify -x -D 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org' -w pau -f pr.mod2.ldif 

Anna modifica homephone de pau
ldapmodify -x -D 'cn=Anna Pou,ou=usuaris,dc=edt,dc=org' -w anna -f pr.mod2.ldif 
