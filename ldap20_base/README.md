# LDAP
## @ Alejandro lopez

### Curs 2020-2021

Repositorio de ldap 2020

Podéis encontrar las imagenes docker al dockerhub [zeuslawl] (https://hub.docker.com/u/zeuslawl/)

ASIX M06-ASO Escola del treball de barcelona

Imagen:

**isx43567395/ldap20_base** Imagen base de un servidor ldap que funciona con detach. carga la base edt.org, los elementos básicos y los usuarios básicos.

Interactivo:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it zeuslawl/ldap20:base /bin/bash
```

Detach:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/ldap20:base 

