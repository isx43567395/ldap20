# LDAP
## @ Alejandro lopez

### Curs 2020-2021

Repositorio de ldap 2020

Podéis encontrar las imagenes docker al dockerhub [zeuslawl] (https://hub.docker.com/u/zeuslawl/)

ASIX M06-ASO Escola del treball de barcelona

**isx43567395/ldap20_base** Imagen base de un servidor ldap que funciona con detach. carga la base edt.org, los elementos básicos y los usuarios básicos.

**isx43567395/ldap20_editat** Imagen base editada ahora de tipo mdb, con un passwd de manager cifrado y de un unico fichero ldif con toda la bd.

**isx43567395/ldap20_acl** Imagen para hacer pruebas de modificación de las acls usando ficheros de modificación. Se ha incorporado la BD cd=config para la administración del servidor dinamicamente.

**isx43567395/ldap20_group** Imagen definitva. monta LDAP
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/ldap20:group
 
