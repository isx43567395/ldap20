# LDAP SERVER
## @isx43567395
### Servidor LDAP

Repositorio ldap 2020

Podéis encontrar las imagenes docker al dockerhub [zeuslawl] (https://hub.docker.com/u/zeuslawl/)

ASIX M06-ASO Escola del treball de barcelona

Imagen:

**isx43567395/ldap20_entrypoint** Imagen con varias opciones de arranque
start,initdb,initdbedt


 * initedt crea toda la base de datos edt (borra todo lo que hay antes)
 * initdb Borra todo lo que hay y crea la base de datos sin xixa.
 * start, enciende el servidor 

Detach:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 --net 2hisix -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap  -d zeuslawl/ldap20:entrypoint start

docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 --net 2hisix -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap  -d zeuslawl/ldap20:entrypoint initdb

docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 --net 2hisix -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap  -d zeuslawl/ldap20:entrypoint initdbedt


 
